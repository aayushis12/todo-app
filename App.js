import React from 'react';
import { StyleSheet, View,Text, TextInput, Button } from 'react-native';
import {Provider} from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import AddTodo from './addTodo';
import reducers from './reducers';

const store = createStore(reducers,{}, applyMiddleware(reduxThunk));

export default class App extends React.Component {

  render() {
    return (
      <Provider store={store}>
        <AddTodo/>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: '14%',
    alignItems: 'center',
  },
  inputBoxStyle: {
    height: 40,
    width: '80%',
    borderColor: 'gray',
    borderWidth: 1,
  },
});
