export const addTodo = (todo) => ({
	type: "add todo", 
	payload: todo,
})

export const deleteTodo = index => ({
	type: "delete todo",
	payload: index,
})

export const updateTodo = newTodo => ({
	type: "update todo",
	payload: newTodo,
})